#!/usr/bin/python3
import configparser
import os.path
from pprint import pprint
import argparse
try:
    import ipgettercarlos as ipgetter
except ImportError:
    import ipgetter
import logging
from logging import handlers
import subprocess
import sys
from geolite2 import geolite2
import json
import requests
import urllib3
from pynetlinux import route,ifconfig

debug = False
local = False
quiet = False

def cidr_to_netmask(cidr):
  cidr = int(cidr)
  mask = (0xffffffff >> (32 - cidr)) << (32 - cidr)
  return (str( (0xff000000 & mask) >> 24)   + '.' +
          str( (0x00ff0000 & mask) >> 16)   + '.' +
          str( (0x0000ff00 & mask) >> 8)    + '.' +
          str( (0x000000ff & mask)))

def setupLogging():
    global log

    log.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    if debug:
        log.addHandler(ch)

    fh = handlers.RotatingFileHandler('{}log/notify.log'.format(root), maxBytes=(1048576 * 5), backupCount=4)
    fh.setFormatter(formatter)
    log.addHandler(fh)

def getScriptDirectory():
    path = os.path.realpath(sys.argv[0])
    if os.path.isdir(path):
        return path+os.sep
    else:
        return os.path.dirname(path)+os.sep

if not os.path.isfile('/usr/local/oiremote/settings.ini'):
    pprint('settings.ini is missing. exiting!')
    exit(1)

root = getScriptDirectory()
log = logging.getLogger(__name__)
setupLogging()
config = None

parser = argparse.ArgumentParser(prog='notify_server', usage='%(prog)s [options]')
parser.add_argument('-l', '--local', action='store_true', help='Run script in local test mode')
parser.add_argument('-q', '--quiet', action='store_true', help='Output errors only')
parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')
args = parser.parse_args()
local = args.local
quiet = args.quiet
debug = args.verbose

if not quiet:
    print("Notifying the OiRemote server with my status")

try:
    config = configparser.ConfigParser()
    config.read_file(open('/usr/local/oiremote/settings.ini'))
except FileNotFoundError:
    log.error('Settings.ini not found. Exiting!')
    exit(1)

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

data = dict()

data['key'] = config.get('main', 'key')
data['st'] = None
data['lat'] = None
data['long'] = None
data['tz'] = None
data['ctry'] = None
data['locip'] = None
data['locsn'] = None
data['locgw'] = None
data['locmac'] = None
data['stcip'] = None
data['stcsn'] = None
data['ver'] = None
data['vpnip'] = None
data['vpnsn'] = None
data['vpnmac'] = None
data['vpnsetup'] = True

try:
    eth0 = ifconfig.Interface(b'eth0')
    data['locip'] = eth0.get_ip()
    data['locsn'] = cidr_to_netmask(eth0.get_netmask())
    data['locmac'] = eth0.get_mac()
    data['locgw'] = route.get_default_gw()
except OSError:
    log.warn('Network interface eth0 is unavailable.')
    pass

try:
    eth1 = ifconfig.Interface(b'eth0.1')
    data['stcip'] = eth1.get_ip()
    data['stcsn'] = cidr_to_netmask(eth1.get_netmask())
except OSError:
    log.warn('Network interface eth0.1 is unavailable.')
    pass

try:
    vpn = ifconfig.Interface(b'vpn_vnic')
    data['vpnip'] = vpn.get_ip()
    data['vpnsn'] = cidr_to_netmask(vpn.get_netmask())
    data['vpnmac'] = vpn.get_mac()
except OSError:
    log.warn('Network interface vpn_vnic is unavailable.')
    data['vpnsetup'] = False
    pass

try:
    data['ver'] = open('update_ver').readline().rstrip()
except OSError:
    log.warn('Unable to get OiRemote version')
    pass

data['uptime'] = subprocess.Popen("cat /proc/uptime | cut -d ' ' -f 1", shell=True, stdout=subprocess.PIPE).stdout.read().rstrip().decode("utf-8")
data['idletime'] = subprocess.Popen("cat /proc/uptime | cut -d ' ' -f 2", shell=True, stdout=subprocess.PIPE).stdout.read().rstrip().decode("utf-8")

try:
    myip = ipgetter.myip()
    if myip is '':
        print("none")
        exit(1)

    reader = geolite2.reader()
    info = reader.get(myip)
    geolite2.close()
    if info is not None:
        data['st'] = info['subdivisions'][0]['iso_code']
        data['lat'] = str(info['location']['latitude'])
        data['long'] = str(info['location']['longitude'])
        data['tz'] = info['location']['time_zone']
        data['ctry'] = info['country']['iso_code']

except:
    log.warn('Unable to get geo location info.')
    pass

if debug:
    log.info("Data to send: {0}".format(json.dumps(data)))

try:
    headers = {'Content-Type':'application/json'}
    req = requests
    if local:
        res = req.request('POST', 'http://192.168.14.100:8000/remote/update', verify=False, headers=headers, allow_redirects=True, data=json.dumps(data))
    else:
        res = req.request('POST', config.get('main','notify'), verify=False, headers=headers, allow_redirects=True, data=json.dumps(data))

    if debug:
        print()
        print("RESPONSE:")
        pprint(res.text)

    try:
        js = json.loads(res.text)

        if js['code'] is not 202:
            log.error('API code: {0} Error: {1}'.format(js['code'], js['error']))
            exit(1)

    except ValueError as ve:
        log.error('Response Error: {}'.format(ve))
        exit(1)

except requests.ConnectionError as ce:
    pprint(ce)
    log.error('Unable to update the server. Connection failed!')
    exit(1)

if debug:
    log.info("Server Notified")

if not quiet:
    print("Server Notified")

exit(0)