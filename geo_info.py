#!/usr/bin/python3

import ipgetter
from geolite2 import geolite2

try:
    myip = ipgetter.myip()
    if myip is '':
        print("none")
        exit(1)

    reader = geolite2.reader()
    info = reader.get(myip)
    geolite2.close()
    if info is not None:
        st = info['subdivisions'][0]['iso_code']
        lat = info['location']['latitude']
        long = info['location']['longitude']
        tz = info['location']['time_zone']
        ctry = info['country']['iso_code']

        print(str(myip) + ":" + str(lat) + ":" + str(long) + ":" + str(st) + ":" + str(ctry) + ":" + str(tz))
    else:
        print('none')
except:
    print('none')