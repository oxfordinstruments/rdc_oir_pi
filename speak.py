#!/usr/bin/python3
# Evotodi 10/16/2014
#Speaks the ip address for eth upon boot

import socket
import fcntl
import struct
import os
import time

text = None

def get_ip_address(ifname):
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	return socket.inet_ntoa(fcntl.ioctl(
		s.fileno(),
		0x8915, # SIOCGIFADDR
		struct.pack('256s', ifname[:15])
	)[20:24])

def make_text(eth):
	global text
	try:
		ethip = get_ip_address(eth)
	except OSError as ose:
		print("speak.py: eth0 ip not available")
		return False
	ethiparr = ethip.split('.')
	for oct in ethiparr:
		for dig in oct:
			text += dig + " "
		text += ", dot, "
	text = text[:-5]
	return True

text = "I P address for.  e t h 0 is. "
if not make_text(b"eth0"):
	time.sleep(10)
	if not make_text(b"eth0"):
		time.sleep(10)
		if not make_text(b"eth0"):
			time.sleep(10)
			if not make_text(b"eth0"):
				exit(1)

if os.path.isfile("/rdc/oir_enable"):
	sts = os.popen("/usr/local/oiremote/./status setup")
	sts = sts.readline()
	text += ",. ,. O. I. Remote Status is., " + sts
else:
	text += ",. ,. O. I. Remote is, dis. abled.,"

if os.path.isfile("/rdc/oiv_enable"):
	text += ",. ,. O. I. Vish en is, enabled., "
else:
	text += ",. ,. O. I. Vish en is., Dis. abled.,"


out = os.popen("echo " + '"' + text + '" | flite -voice kal16')
exit(0)