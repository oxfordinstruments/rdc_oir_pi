#!/bin/bash
#Setup this client for connecting to the OiRemote network

#Vars
INI="/usr/local/oiremote/settings.ini"
NOTIFY="$(sed -n -e 's/^\s*notify\s*=\s*//p' $INI)"
OIR_PATH="$(sed -n -e 's/^\s*path\s*=\s*//p' $INI)"
VPNC_PATH="$(sed -n -e 's/^\s*vpncpath\s*=\s*//p' $INI)"
KEY="$(sed -n -e 's/^\s*key\s*=\s*//p' $INI)"
LOG_FILE="$OIR_PATH/log/log.txt"



USERNAME=""
PASSWORD=""
HUB=""
OISERVER=""
ACCOUNT="oiremote"
OPTIND=1  # Reset in case getopts has been used previously in the shell.

function usage()
{
	echo ""
	echo "Usage: setup -u username -p password -a hub -s server:port"
	echo "-u   The username for this client"
	echo "-p   The pasword for this client"
	echo "-h   The hub for this client"
	echo "-s   The server and optional port (server:port)"
	echo "-a   The optional account to use (default: oiremote)"
	echo ""
	exit 1
}

function log()
{
	echo $(date)"  SETUP: "$@ >> $LOG_FILE
}

if test "$1" == "" ; then
	usage
fi

#Get arguments
while getopts "u:p:h:s:a:" opt; do
    case "$opt" in
    u)  USERNAME=$OPTARG
        ;;
    p)  PASSWORD=$OPTARG
        ;;
	h)	HUB=$OPTARG
		;;
	s)	OISERVER=$OPTARG
		;;
	a)	ACCOUNT=$OPTARG
		;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

printf "\n"

#check if user is root
if [ "$(whoami)" != "root" ]; then
	printf "Setup must be run as root.\nCannot continue!\n\n"
	exit 1
fi

#check if username is not blank
if [ $USERNAME == "" ]; then
	printf "Username cannot be blank."
	usage
fi

#check of password is blank
if [ $PASSWORD == "" ]; then
	printf "Password cannot be blank."
	usage
fi

#check if accont is blank
if [ $HUB == "" ]; then
	printf "Hub cannot be blank"
	usage
fi

#remove setup lock file
rm $OIR_PATH/setup_complete

#restore the origional vpn_client.config
cp -f $OIR_PATH/vpn_client.config.orig $VPNC_PATH/vpn_client.config

#check if vpnclient is exists and is running
if [ ! -f $VPNC_PATH/vpnclient ]; then
	printf "vpnclient binary not found!\nCannot continue!\n\n"
	log "vpnclient binary not found!"
	exit 1
fi

if [ "$(ps -e | grep -c vpnclient)" = 0 ]; then
	printf "vpnclient is not running... attempting to start it.\n\n"
	service vpnclient start
	sleep 3
	if [ "$(ps -e | grep -c vpnclient)" = 0 ]; then
		printf "vpnclient did not start in the allowed time.\nCannot continue!\n\n"
		log "vpnclient did not start in the allowed time."
		exit 1
	fi
else
	printf "vpnclient is running\n\n"
fi

#check if vpncmd exists
if [ ! -f $VPNC_PATH/vpncmd ]; then
	printf "vpncmd binary not found.\nCannot continue!"
	log "vpncmd binary not found."
	exit 1
fi

if [ "$OISERVER" == "" ]; then
	$OIR_PATH/./setup.exp $USERNAME $PASSWORD $HUB $ACCOUNT
else
	$OIR_PATH/./setup.exp $USERNAME $PASSWORD $HUB $ACCOUNT $OISERVER
fi

#check if interfaces is setup correctly
if [ "$(cat /etc/network/interfaces | grep -c vpn_vnic)" = 0 ]; then
	cat $OIR_PATH/interfaces.add >> /etc/network/interfaces
	dhclient vpn_vnic
fi

#renew the ip address
printf "\n\nRenewing the ip address..."
dhclient vpn_vnic

IPADDY="$(ifconfig vpn_vnic | sed -n '2 p' | awk '{print $2}' | cut -d : -f 2)"
printf "\nIP Address: "$IPADDY
printf "\n\n"

#check status of the vpn
COUNTER=1
while [  $COUNTER -lt 10 ]; do
	STATUS_RTN="$($OIR_PATH/./status setup | grep -ic 'Established')"
	if [ $STATUS_RTN == 1 ]; then
	   	$OIR_PATH/./status setup
	    break
	fi

	echo "Retry" $COUNTER
	let COUNTER=COUNTER+1
	sleep 5
done

printf "\n\nPinging the vpn server...\n\n"

ping -c 3 14.25.36.1

printf "\n\n------------------------------------------"
printf "\nUsername: "$USERNAME
printf "\nHub: "$HUB
printf "\nIP Address: "$IPADDY

if [ "$OISERVER" != "" ]; then
	printf "\nServer: "$OISERVER
fi

printf "\n\nContact Support to verify the connection"
printf "\n-------------------------------------------"
printf "\n\nSetup Complete\n\n"
log "Setup complete"
touch $OIR_PATH/setup_complete
$OIR_PATH/./notify_server
exit 0
